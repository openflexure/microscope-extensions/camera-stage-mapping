# Contributing to the project
We would welcome contributions to the module, to make it work with different hardware, extend its functionality, and validate its performance.  There are plenty ways to contribute, not all of which require you to write Python code.

## Testing it out
This module is designed to calibrate the relationship between a camera and a translation stage.  In order to make it work well, it should be tested on many different systems (mostly microscopes).  If you have run this on your microscope and are willing to share the calibration results and some details of your system, that would be enormously helpful - please get in touch by raising an [issue] on our GitLab repository.

## Reporting bugs
We have no doubt that there will be bugs and quirks in the module, particularly when it's used with hardware that is different from the hardware we have used during development.  Do let us know if things go wrong by raising an [issue].

## Contributing code
If you have fixed problems or improved functionality, it would be great to receive a merge request with improved code.  Please add your name to ``CONTRIBUTORS.md`` and make an entry in ``CHANGELOG.md``.

## Developing the module
We manage dependencies and packaging with [poetry].  After installing poetry, clone this repository and run ``poetry install`` to set up your development environment.  Note that, by default, this will install in an isolated virtual environment managed by [poetry].  If you want to use this module as part of a larger project, it's best to install using ``pip`` either from a wheel built using ``poetry build`` or from a published version of the package, once it exists.
```
git clone https://gitlab.com/openflexure/microscope-extensions/camera-stage-mapping.git
cd camera-stage-mapping
poetry install
```
Note that there are a number of optional dependencies, including OpenCV (required for ``direct`` correlation tracking), ipython/matplotlib (required for the example notebooks), and the OpenFlexure Microscopy python client (to test with an OpenFlexure Microscope).  These can be installed by specifying ``correlation``, ``ipython``, or ``ofmclient`` "extras" when running ``poetry install``.

## Wish list
Check out the [issue] list for things that could do with fixing; one major nice-to-have would be a simulated microscope that would allow testing of both the image tracking and the analysis code.

## Release procedure
Releases are semi-automatic using Gitlab CI/CD.  To make a new release:
* Merge any features in to ``master``
* Update ``CHANGELOG.md``
* Increment the version in ``pyproject.toml``
* Tag the commit with ``v?.?.?``
* Push the tag to Gitlab
This will automatically upload the package to pypi, and should also rebuild documentation on readthedocs via an API hook.


[issue]: https://gitlab.com/openflexure/microscope-extensions/camera-stage-mapping/-/issues
[poetry]: https://python-poetry.org/