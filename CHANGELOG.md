# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
We try to follow [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
*Please add all changes not yet in a release here, to make it simpler when we release them*.

## [0.1.7]
* Relax dependencies to allow numpy 2
* Reduce range of motion to improve reliability
* Return to starting position on failure

## [0.1.6]
* Added some type hints
* Added a `logger` argument to allow real-time log propagation when calibrating in a LabThing

## [0.1.5]
* Replaced all instances of `np.float` with `float` for compatibility with newer `numpy` versions
* Updated dependencies, including upgrading Python requirement to 3.9

## [0.1.4] - 2020-09-04
* Minor updates to OFM extension
* Fixed homepage link for pypi package

## [0.1.3] - 2020-07-15
* Added release procedures to ``CONTRIBUTING``
* Removed an unused import to ensure compatibility with future versions of LabThings

## [0.1.2] - 2020-07-01
### Added
* Working documentation, using ``sphinx``, ``sphinx-autoapi``, and readthedocs
* Readme, changelog, and contributing files
* Documentation on readthedocs

## [0.1.1]
### Added
* Example plotting code
* Example calibration data as JSON files
* ``Tracker`` now records timestamps at each point
### Changed
* Various refactorings and tidying-up
* FFT tracking is moved to a separate module
* ``wait`` function is bundled into the extension more nicely
* The OFM extension records timing data for every move
* Added ``time`` to the ``TrackerHistory`` tuple
* Upgraded to LabThings 0.7 format for the OFM extension


## [0.1] - 2020-05-07
This was the first version in approximately package-like shape.  It featured:
* 1D and 2D calibrations using self-contained functions
* ``numpy`` wrappers for the OpenFlexure Microscope client
* Combining 1D calibrations into a 2D affine transform

## Older code
This package started as a loose collection of scripts and notebooks:
* Client for the OpenFlexure Microscope (now ``openflexure-microscope-pyclient``)
* Calibration using the old ``nplab`` four-point method
* Wrappers to run ``nplab`` code with an openflexure microscope